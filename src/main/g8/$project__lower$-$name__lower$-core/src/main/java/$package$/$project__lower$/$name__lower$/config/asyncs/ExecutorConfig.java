package $package$.$project;format="lower"$.$name;format="lower"$.config.asyncs;

import io.opentracing.Tracer;
import io.opentracing.contrib.concurrent.TracedExecutor;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurerSupport;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

@EnableAsync
@Configuration
public class ExecutorConfig extends AsyncConfigurerSupport {

  public static final String ASYNC_$project;format="upper"$ = "ASYNC_$project;format="UPPER"$";

  private final BeanFactory beanFactory;
  private final Tracer tracer;
  private final Integer asyncCorePoolSize;
  private final Integer asyncMaxPoolSize;

  public ExecutorConfig(BeanFactory beanFactory,
                        Tracer tracer,
                        @Value("\${async.default.thread.corepoolsize:8}") Integer asyncCorePoolSize,
                        @Value("\${async.default.thread.maxPoolSize:200}") Integer asyncMaxPoolSize) {
    this.beanFactory = beanFactory;
    this.tracer = tracer;
    this.asyncCorePoolSize = asyncCorePoolSize;
    this.asyncMaxPoolSize = asyncMaxPoolSize;
  }

  @Override
  public Executor getAsyncExecutor() {
    ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
    threadPoolTaskExecutor.setCorePoolSize(asyncCorePoolSize);
    threadPoolTaskExecutor.setMaxPoolSize(asyncMaxPoolSize);
    threadPoolTaskExecutor.setThreadNamePrefix(ASYNC_$project;format="upper"$);
    threadPoolTaskExecutor.initialize();
    return new TracedExecutor(threadPoolTaskExecutor, tracer, false);
  }

}
