package $package$.$project;format="lower"$.$name;format="lower"$.exceptions;

import lombok.extern.slf4j.Slf4j;
import pe.interbank.commons.CatalogErrors;
import pe.interbank.commons.exception.GenericRuntimeException;
import pe.interbank.commons.exception.InternalException;

@Slf4j
public enum $name;format="Camel"$CatalogErrors implements CatalogErrors {

  $name;format="upper"$_E500_001(InternalException.class, "Error desconocido");

  private final GenericRuntimeException exception;

  $name;format="Camel"$CatalogErrors(Class<? extends GenericRuntimeException> exceptionClass, String systemMessage) {
    exception = getException(exceptionClass, systemMessage, name());
  }

  @Override
  public GenericRuntimeException getException() { return exception; }


}


