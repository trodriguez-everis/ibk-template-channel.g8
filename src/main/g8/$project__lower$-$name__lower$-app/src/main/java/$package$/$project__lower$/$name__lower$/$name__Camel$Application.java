package $package$.$project;format="lower"$.$name;format="lower"$;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "pe.interbank")
public class $name;format="Camel"$Application {

  public static void main(String[] args) {
    SpringApplication.run($name;format="Camel"$Application.class, args);
  }

}
