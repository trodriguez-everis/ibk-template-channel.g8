package $package$.$project;format="lower"$.$name;format="lower"$.$module1;format="lower"$.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient
public interface $module1;format="Camel"$Client {

  @GetMapping()
  String find(@RequestBody String body);

}
