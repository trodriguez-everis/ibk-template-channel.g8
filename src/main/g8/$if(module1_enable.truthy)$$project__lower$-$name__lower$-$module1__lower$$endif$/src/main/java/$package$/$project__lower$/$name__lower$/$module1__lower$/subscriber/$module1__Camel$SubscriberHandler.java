package $package$.$project;format="lower"$.$name;format="lower"$.$module1;format="lower"$.subscriber;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pe.interbank.commons.pubsub.SubscriberHandler;
import pe.interbank.commons.pubsub.subscriber.util.SubscriberMessageActions;

import $package$.$project;format="lower"$.$name;format="lower"$.$module1;format="lower"$.facade.$module1;format="Camel"$Facade;

import java.nio.charset.StandardCharsets;
import java.util.Map;

@Component
@Slf4j
public class $module1;format="Camel"$SubscriberHandler implements SubscriberHandler {

  private AuditService auditService;
  private static final String AUDIT_SUBSCRIBER_PREFIX = "ibk.common.audit";

  private final $module1;format="Camel"$Facade $module1;format="camel"$Facade;

  public $module1;format="Camel"$SubscriberHandler($module1;format="Camel"$Facade $module1;format="camel"$Facade) {
    this.$module1;format="camel"$Facade = $module1;format="camel"$Facade;
  }

  @Override
  public String getPropertyBase() {
    return AUDIT_SUBSCRIBER_PREFIX;
  }

  @Override
  public SubscriberMessageActions handler(Map<String, String> headers, byte[] message) {
    log.info("AuditSubscriberSubscriber executing");
    String jsonMessage = new String(message, StandardCharsets.UTF_8);
    log.info("message: {}", jsonMessage);
    return SubscriberMessageActions.COMPLETE;
  }
}
