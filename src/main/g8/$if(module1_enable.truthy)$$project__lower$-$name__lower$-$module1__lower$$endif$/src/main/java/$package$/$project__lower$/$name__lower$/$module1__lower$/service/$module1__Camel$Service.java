package $package$.$project;format="lower"$.$name;format="lower"$.$module1;format="lower"$.service;

import org.springframework.stereotype.Component;

import $package$.$project;format="lower"$.$name;format="lower"$.$module1;format="lower"$.client.$module1;format="Camel"$Client;

@Component
public class $module1;format="Camel"$Service {

  private final $module1;format="Camel"$Client $module1;format="camel"$Client;

  public $module1;format="Camel"$Service($module1;format="Camel"$Client $module1;format="camel"$Client) {
    this.$module1;format="camel"$Client = $module1;format="camel"$Client;
  }

  public String find(String body) {
    return $module1;format="camel"$Client.find();
  }

}
