package $package$.$project;format="lower"$.$name;format="lower"$.$module1;format="lower"$.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import $package$.$project;format="lower"$.$name;format="lower"$.$module1;format="lower"$.facade.$module1;format="Camel"$Facade;

@RestController
@RequestMapping("/$module1;format="lower"$")
public class $module1;format="Camel"$Controller {

  private final $module1;format="Camel"$Facade $module1;format="camel"$Facade;

  public $module1;format="Camel"$Controller($module1;format="Camel"$Facade $module1;format="camel"$Facade) {
    this.$module1;format="camel"$Facade = $module1;format="camel"$Facade;
  }

  @GetMapping("/")
  public String find(@RequestBody String body) {
    return $module1;format="camel"$Facade.find(body);
  }

}
