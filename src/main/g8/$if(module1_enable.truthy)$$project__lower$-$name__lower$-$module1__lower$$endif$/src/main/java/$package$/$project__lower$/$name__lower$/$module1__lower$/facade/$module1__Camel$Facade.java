package $package$.$project;format="lower"$.$name;format="lower"$.$module1;format="lower"$.facade;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import $package$.$project;format="lower"$.$name;format="lower"$.$module1;format="lower"$.service.$module1;format="Camel"$Service;

@Component
public class $module1;format="Camel"$Facade {

private final $module1;format="Camel"$Service $module1;format="camel"$Service;

  public $module1;format="Camel"$Facade($module1;format="Camel"$Service $module1;format="camel"$Service) {
    this.$module1;format="camel"$Service = $module1;format="camel"$Service;
  }

  public String find(@RequestBody String body) {
    return $module1;format="camel"$Service.find(body);
  }

}
